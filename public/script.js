const videoPlayer = document.querySelector('.video-player')
const video = videoPlayer.querySelector('.video')
const playButton = videoPlayer.querySelector('.play-button')
const volume = videoPlayer.querySelector('.volume')
const currentTimeElement = videoPlayer.querySelector('.current')
const durationTimeElement = videoPlayer.querySelector('.duration')
const progress = videoPlayer.querySelector('.video-progress')
const progressBar = videoPlayer.querySelector('.video-progress-filled')
const mute =videoPlayer.querySelector('.mute')
const fullscreen = videoPlayer.querySelector('.fullscreen')


playButton.addEventListener('click', (e) => {
    if(video.paused){
      video.play()
      e.target.textContent = '❚❚'
    } else {
      video.pause()
      e.target.textContent = '►'
    }
  })
  //volume
  volume.addEventListener('mousemove',(e)=>{
    video.volume=e.target.value
  })
 // the current time / the duration time 
  const currentTime = ()=>{
    let currentMinutes=Math.floor(video.currentTime/60)
    let currentSecondes=Math.floor(video.currentTime - currentMinutes*60)
    let durationMinutes=Math.floor(video.duration/60)
    let durationSecondes=Math.floor(video.duration - durationMinutes*60)
    currentTimeElement.innerHTML=`${currentMinutes} : ${currentSecondes < 10 ?'0'+currentSecondes:currentSecondes}`
    durationTimeElement.innerHTML=`${durationMinutes} : ${durationSecondes}`
 
  }
  video.addEventListener('timeupdate',currentTime)
  //progress bar
video.addEventListener('timeupdate',()=>{
  const percentage=(video.currentTime / video.duration) * 100
  progressBar.style.width = `${percentage}%`
})
// change the progress bar 
progress.addEventListener('click',(e)=>{
  //The offsetX read-only property of the MouseEvent interface provides the offset in the X coordinate of the mouse pointer between that event and the padding edge of the target node.
  //The HTMLElement.offsetWidth read-only property returns the layout width of an element as an integer.
const progressTime =(e.offsetX / progress.offsetWidth)*video.duration
video.currentTime =progressTime

})
// mute button
mute.addEventListener('click',()=>{
  video.muted = !video.muted;
  mute.classList.toggle('muted')
})


fullscreen.addEventListener('click',()=>{

  video.requestFullscreen();
})



